import { Injectable } from '@nestjs/common';

import { Email } from './email';

import { CrashDump } from './crash-dump';
import { MailSettings } from './mail-settings';
import { MailerService } from './mailer.service';

@Injectable()
export class CrashDumpMailerService {
  constructor(
    private mailerSvc: MailerService,
    private settings: MailSettings) {
  }

  send(crashDump: CrashDump): Promise<object[]> {
    const email = new Email();

    email.addTo(this.settings.crashDumpTo);
    email.setFrom(this.settings.crashDumpFrom);
    email.setSubject(this.settings.crashDumpSubject);
    email.addAttachment(JSON.stringify(crashDump, null, 2),
      'application/json', 'crashdump.json');

    return this.mailerSvc.send(email);
  }
}
