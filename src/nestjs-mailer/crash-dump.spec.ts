import { Request } from 'express';

import { CrashDump } from './crash-dump';

describe('CrashDump()', () => {
  describe('.set request()', () => {
    let request: Request;
    let cd: CrashDump;

    beforeEach(() => {
      cd = new CrashDump();

      request = {
        headers: {
        },
        query: {
        },
        body: {
        },
        params: {
        },
        originalUrl: '/foos/1/bars/2?some=qps&go=here',
        protocol: 'https',
      } as Request;
    });

    it('redacts "cookie" and "authorization" headers.', () => {
      request.headers = {
        host: 'local',
        authorization: 'token here',
        cookie: 'session cookie here',
      };

      cd.request = request;

      expect(cd.headers.host).toBe('local');
      expect(cd.headers.authorization).toBe('<REDACTED>');
      expect(cd.headers.cookie).toBe('<REDACTED>');
    });

    it('redacts sensitive top-level info in an object.', () => {
      request.body = {
        password: 'pass',
        resetToken: 'rt',
        tokenExpires: 'rt',
      };

      cd.request = request;

      expect(cd.body.password).toBe('<REDACTED>');
      expect(cd.body.resetToken).toBe('<REDACTED>');
      expect(cd.body.tokenExpires).toBe('<REDACTED>');
    });

    it('skips nulls.', () => {
      // typeof null is "object" but keys with a value of null are not skipped
      // from the iteration.
      request.body = {
        tokenExpires: 'rt',
        foo: null,
      };

      cd.request = request;

      expect(cd.body.tokenExpires).toBe('<REDACTED>');
      expect(cd.body.foo).toBe(null);
    });

    it('redacts sensitive top-level info in an array.', () => {
      request.body = [{
        password: 'pass',
        resetToken: 'rt',
        tokenExpires: 'rt',
      }];

      cd.request = request;

      expect(cd.body[0].password).toBe('<REDACTED>');
      expect(cd.body[0].resetToken).toBe('<REDACTED>');
      expect(cd.body[0].tokenExpires).toBe('<REDACTED>');
    });

    it('redacts sensitive nested info in an object.', () => {
      request.body = {
        user: {
          password: 'pass',
          resetToken: 'rt',
          tokenExpires: 'rt',
        },
      };

      cd.request = request;

      expect(cd.body.user.password).toBe('<REDACTED>');
      expect(cd.body.user.resetToken).toBe('<REDACTED>');
      expect(cd.body.user.tokenExpires).toBe('<REDACTED>');
    });

    it('redacts sensitive nested info in an array.', () => {
      request.body = {
        users: [{
          password: 'pass',
          resetToken: 'rt',
          tokenExpires: 'rt',
        }],
      };

      cd.request = request;

      expect(cd.body.users[0].password).toBe('<REDACTED>');
      expect(cd.body.users[0].resetToken).toBe('<REDACTED>');
      expect(cd.body.users[0].tokenExpires).toBe('<REDACTED>');
    });

    it('strips the query params off of the url.', () => {
      cd.request = request;

      expect(cd.url).toBe('/foos/1/bars/2');
    });
  });

  describe('.set error()', () => {
    let cd: CrashDump;
    beforeEach(() => {
      cd = new CrashDump();
    });

    it('stores the message and name.', () => {
      cd.error = new Error('foo');

      expect(cd.exception.name).toBe('Error');
      expect(cd.exception.message).toBe('foo');
    });

    it('splits the stack trace into an array.', () => {
      cd.error = new Error('foo');

      expect(Array.isArray(cd.exception.stack)).toBe(true);
    });
  });
});
