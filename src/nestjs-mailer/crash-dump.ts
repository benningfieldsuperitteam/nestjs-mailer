import { Request } from 'express';

type keyValType = {[key: string]: any};

/**
 * Helper class that's used when an unhandled exception occurs.  It attempts to
 * redact any sensitive request information, and captures as much as possible
 * about the error.
 */
export class CrashDump {
  headers: keyValType;
  query: keyValType;
  body: keyValType;
  params: keyValType;
  authUser: keyValType;
  url: string;
  method: string;
  protocol: string;
  exception: {
    message: string;
    name: string;
    stack: string[];
  };
  time: Date = new Date();

  set request(request: Request) {
    this.headers = this.redactSensitiveInfo(
      Object.assign({}, request.headers));

    this.query = this.redactSensitiveInfo(
      Object.assign({}, request.query));

    this.body = this.redactSensitiveInfo(
      Object.assign({}, request.body));

    this.params = this.redactSensitiveInfo(
      Object.assign({}, request.params));

    this.authUser = this.redactSensitiveInfo(
      Object.assign({}, (request as any).user));

    // The URL is captured with the query parameters stripped.  The query
    // parameters are stored above and redacted.  (The query params are
    // stripped here because they may contain sensitive information in string
    // form.)
    this.url      = request.originalUrl.replace(/\?.*$/, '');
    this.method   = request.method; // Verb.
    this.protocol = request.protocol;
  }

  set error(err: Error) {
    this.exception = {
      message: err.message,
      name: err.name,
      stack: err.stack.split('\n'),
    };
  }

  redactSensitiveInfo(obj: keyValType): keyValType {
    const sensitive = [
      'authorization',
      'password',
      'resetToken',
      'tokenExpires',
      'cookie',
    ];

    if (typeof obj !== 'object' || obj === null)
      return obj;

    if (Array.isArray(obj)) {
      for (const val of obj)
        this.redactSensitiveInfo(val);

      return obj;
    }

    sensitive
      .forEach(key => {
        if (obj[key])
          obj[key] = '<REDACTED>';
      });

    for (const key in obj)
      this.redactSensitiveInfo(obj[key]);

    return obj;
  }
}
