import { Email } from './email';

describe('Email()', () => {
  let email: Email;

  beforeEach(() => {
    email = new Email();
  });

  describe('.validateEmail()', () => {
    it('throws a ValidationError if the email is invalid.', () => {
      try {
        email.validateEmail('foo', 'to');
        expect(true).toBe(false);
      }
      catch (err) {
        expect(err.name).toBe('ValidationError');
        expect(err.message).toBe('"to" must be a valid email address.');
        expect(err.field).toBe('Email.to');
      }
    });
  });

  describe('.addTo()', () => {
    it('adds the recipient.', () => {
      email.addTo('foo@bar.com');
      email.addTo('baz@boo.com');

      expect(email.getTo()).toBe('foo@bar.com, baz@boo.com');
    });

    it('throws an error if the recipient is invalid.', () => {
      try {
        email.addTo('foo');
        expect(true).toBe(false);
      }
      catch (err) {
        expect(err.name).toBe('ValidationError');
      }
    });
  });

  describe('.addCC()', () => {
    it('adds the recipient.', () => {
      email.addCC('foo@bar.com');
      email.addCC('baz@boo.com');

      expect(email.getCC()).toBe('foo@bar.com, baz@boo.com');
    });

    it('throws an error if the recipient is invalid.', () => {
      try {
        email.addCC('foo');
        expect(true).toBe(false);
      }
      catch (err) {
        expect(err.name).toBe('ValidationError');
      }
    });
  });

  describe('.addBCC()', () => {
    it('adds the recipient.', () => {
      email.addBCC('foo@bar.com');
      email.addBCC('baz@boo.com');

      expect(email.getBCC()).toBe('foo@bar.com, baz@boo.com');
    });

    it('throws an error if the recipient is invalid.', () => {
      try {
        email.addBCC('foo');
        expect(true).toBe(false);
      }
      catch (err) {
        expect(err.name).toBe('ValidationError');
      }
    });
  });

  describe('.setFrom()', () => {
    it('sets the sender.', () => {
      email.setFrom('foo@bar.com');

      expect(email.getFrom()).toBe('foo@bar.com');
    });

    it('throws an error if the sender is invalid.', () => {
      try {
        email.setFrom('foo');
        expect(true).toBe(false);
      }
      catch (err) {
        expect(err.name).toBe('ValidationError');
      }
    });
  });

  describe('.addAttachment()', () => {
    it('base64 encodes the content.', () => {
      const buff = Buffer.from('asdf');

      email.addAttachment(buff, 'application/fake', 'fake.file');

      expect(email.getAttachments()[0].content).toBe('YXNkZg==');
    });

    it('converts string-type content arguments to base64.', () => {
      email.addAttachment('asdf', 'application/fake', 'fake.file');

      expect(email.getAttachments()[0].content).toBe('YXNkZg==');
    });
  });
});
