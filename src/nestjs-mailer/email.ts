import { ValidationError } from 'bsy-error';
import { EmailValidator } from 'bsy-validation';

export class Email {
  protected to: string[];
  protected cc: string[];
  protected bcc: string[];
  protected from: string;
  protected subject: string;
  protected html: string;
  protected attachments: Array<{[key: string]: string}>;

  constructor() {
    this.to          = [];
    this.cc          = [];
    this.bcc         = [];
    this.attachments = [];
    this.subject     = '';
    this.html        = '';
  }

  addTo(to: string): this {
    this.to.push(this.validateEmail(to, 'to'));

    return this;
  }

  getTo(): string {
    return this.to.join(', ');
  }

  addCC(cc: string): this {
    this.cc.push(this.validateEmail(cc, 'cc'));

    return this;
  }

  getCC(): string {
    return this.cc.join(', ');
  }

  addBCC(bcc: string): this {
    this.bcc.push(this.validateEmail(bcc, 'bcc'));

    return this;
  }

  getBCC(): string {
    return this.bcc.join(', ');
  }

  setFrom(from: string): this {
    this.from = this.validateEmail(from, 'from');

    return this;
  }

  getFrom(): string {
    return this.from;
  }

  setSubject(subject: string): this {
    this.subject = subject;

    return this;
  }

  getSubject(): string {
    return this.subject;
  }

  setHtml(html: string): this {
    this.html = html;

    return this;
  }

  getHtml(): string {
    return this.html;
  }

  /**
   * Add an attachment.
   */
  addAttachment(content: Buffer | string, type: string, filename: string): this {
    if (typeof content === 'string')
      content = Buffer.from(content);

    this.attachments.push({
      content: content.toString('base64'),
      type: type,
      filename: filename,
    });

    return this;
  }

  /**
   * Get the attachments (the content is base64 encoded).
   */
  getAttachments(): Array<{[key: string]: string}> {
    return this.attachments;
  }

  /**
   * Validate the email address, and raise a ValidationError if it's invalid.
   */
  validateEmail(email: string, prop: string): string {
    const validator = new EmailValidator();

    if (!validator.validate(email))
      throw new ValidationError(validator.getErrorMessage(prop), `Email.${prop}`);

    return email;
  }
}
