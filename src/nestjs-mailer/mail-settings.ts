export class MailSettings {
  constructor(
    public sendgridAPIKey: string,
    public crashDumpTo: string,
    public crashDumpFrom: string,
    public crashDumpSubject: string,
    public trapTo: string,
  ) { }
}
