import { Injectable } from '@nestjs/common';

import * as sgMail from '@sendgrid/mail';

import { ValidationError } from 'bsy-error';

import { Email } from './email';
import { MailSettings } from './mail-settings';

@Injectable()
export class MailerService {
  constructor(private settings: MailSettings) {
  }

  send(email: Email): Promise<object[]> {
    if (email.getTo().length === 0 && email.getCC().length === 0 && email.getBCC().length === 0) {
      throw new ValidationError('Attempted to send an email with no recipients.', 'Email.to');
    }

    if (!email.getFrom()) {
      throw new ValidationError('Attempted to send an email with no sender.', 'Email.from');
    }

    const message: {[key: string]: any} = {
      to: email.getTo(),
      cc: email.getCC(),
      bcc: email.getBCC(),
      from: email.getFrom(),
      subject: email.getSubject(),
      html: email.getHtml(),
      attachments: email.getAttachments(),
    };

    // Trap the email in non-prod environments.
    if (!process.env.NODE_ENV || !process.env.NODE_ENV.startsWith('prod')) {
      message.html += `
        <div>
          <div>Original to: ${message.to}</div>
          <div>Original cc: ${message.cc}</div>
          <div>Original bcc: ${message.bcc}</div>
        </div>
      `;

      message.to  = this.settings.trapTo;
      message.cc  = '';
      message.bcc = '';
    }

    return sgMail.send(message as any);
  }
}
