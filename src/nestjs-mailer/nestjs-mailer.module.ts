import { DynamicModule, Global, Module } from '@nestjs/common';

import * as sgMail from '@sendgrid/mail';

import { CrashDumpMailerService } from './crash-dump-mailer.service';
import { MailSettings } from './mail-settings';
import { MailerService } from './mailer.service';

@Global()
@Module({})
export class NestJSMailerModule {
  /**
   * Cofiguration for the mailer module.
   * @param sendgridAPIKey API key for SendGrid.
   * @param crashDumpTo To whom crash dump emails should be sent.
   * @param crashDumpSubject Crash dump email subject.
   * @param trapTo Developer email for trapping emails in non-prod environments.
   */
  static forRoot(
    sendgridAPIKey: string,
    crashDumpTo: string,
    crashDumpFrom: string,
    crashDumpSubject: string,
    trapTo: string,
  ): DynamicModule {

    // Configure Sendgrid.
    sgMail.setApiKey(sendgridAPIKey);

    // Provider for mail settings.
    const settings = new MailSettings(sendgridAPIKey, crashDumpTo,
      crashDumpFrom, crashDumpSubject, trapTo);

    const settingsProvider = {
      provide: MailSettings,
      useValue: settings,
    };

    return {
      module: NestJSMailerModule,
      providers: [
        CrashDumpMailerService,
        MailerService,
        settingsProvider,
      ],
      exports: [
        CrashDumpMailerService,
        MailerService,
      ],
    };
  }
}
